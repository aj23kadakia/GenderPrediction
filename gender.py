from nltk import NaiveBayesClassifier,classify
import NameLoader
import random

class genderPrediction():
    #load names from nameloader
    def loadNames(self):
        return NameLoader.getNameList()

    def nameFeat(self,name):
        name = name.upper()
        return {
            'last_letter': name[-1],
            'last_two' : name[-2:],
            'last_three': name[-3:],
            'last_is_vowel' : (name[-1] in 'AEIOUY')
        }
    
    def getProbDistr(self,nameTuple):
            male_prob = (nameTuple[1] * 1.0) / (nameTuple[1] + nameTuple[2])
            if male_prob == 1.0:
                male_prob = 0.99
            elif male_prob == 0.0:
                male_prob = 0.01
            else:
                pass
            female_prob = 1.0 - male_prob
            return (male_prob, female_prob)
    
    def getFeatures(self):
        maleNames,femaleNames = self.loadNames()
        featureset = list()
        
        for nametuple in maleNames:
            features = self.nameFeat(nt[0])
            male_prob, female_prob = self.getProbDistr(nametuple)
            features['male_prob'] = male_prob
            features['female_prob'] = female_prob
            featureset.append((features,'Male'))
        
        for nametuple in femaleNames:
            features = self.nameFeat(nametuple[0])
            male_prob, female_prob = self.getProbDistr(nametuple)
            features['male_prob'] = male_prob
            features['female_prob'] = female_prob
            featureset.append((features,'Female'))
    
        return featureset
    
    def test(self,trainingPercent=0.80):
        featureset = self.getFeatures()
        random.shuffle(featureset)
        name_count = len(featureset)
        cut_point=int(name_count*trainingPercent)
        train_set = featureset[:cut_point]
        test_set  = featureset[cut_point:]
        self.train(train_set)
        return classify.accuracy(self.classifier,test_set)

    #train classifier
    def train(self,train_set):
        self.classifier = NaiveBayesClassifier.train(train_set)
        return self.classifier
    #classify
    def classify(self,name):
        feats = self.nameFeat(name)
        return self.classifier.classify(feats)
    
if __name__ == "__main__":
    gp = genderPrediction()
    accuracy = gp.test()
    print 'Accuracy: %f'%accuracy

    while(True):
        name = raw_input('Enter name: ')
        print '%s : %s'%(name, gp.classify(name))

