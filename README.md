Project DEscription

Gender Prediction App
This project will predict the gender of a person based on the first name given to the system.
The corpus used in this system is from 2010 US census collection which is then formatted into pickle file
The features used to classify are last_letter, last_two, last_three, last_is_vowel to improve the accuracy
The classifier I used for prediction is NLTK's NaiveByesClassifier.
NaiveBayesClassifer is easy to implement in the code and also efficient in terms of giving results.It is based on Bayes Theorem of
Conditional Probability. I have used NaiveBayesClassifier before when I was workingon Social Media Sentimental Analysis and because it widely used classifier for text analysis