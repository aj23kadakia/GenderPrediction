"""
NamesLoader.py
"""
import os
import re
import urllib2
from zipfile import ZipFile
import csv
import pickle
        
def getNameList():
    if not os.path.exists('names.pickle'):
        print 'Generating names.pickle'
        if not os.path.exists('names.zip'):
            print 'Downloading names.pickle file'
            downloadNames()
        else:
            pass
    
        #Extracting names from names.zip
        namesDict=extractNamesDict()
        
        maleNames=list()
        femaleNames=list()
        
        #Sorting Names
        for name in namesDict:
            counts=namesDict[name]
            tuple=(name,counts[0],counts[1])
            if counts[0]>counts[1]:
                maleNames.append(tuple)
            elif counts[1]>counts[0]:
                femaleNames.append(tuple)
        
        names=(maleNames,femaleNames)
        
        #Saving names.pickle
        fw=open('names.pickle','wb')
        pickle.dump(names,fw,-1)
        fw.close()
        #Saved names.pickle
    else:
        f=open('names.pickle','rb')
        names=pickle.load(f)
    return names
    
def downloadNames():
    u = urllib2.urlopen('https://gitlab.com/aj23kadakia/GenderPrediction/blob/master/names.zip')
    localFile = open('names.zip', 'w')
    localFile.write(u.read())
    localFile.close()
    
def extractNamesDict():
    f=ZipFile('names.zip', 'r')
    filenames = f.namelist()  
    names = dict()
    genderMap = {'Male':0,'Female':1}
    
    for filename in filenames:
        file = f.open(filename,'r')
        rows = csv.reader(file, delimiter=',')
        
        for row in rows:
            name = row[0].upper()
            gender = genderMap[row[1]]
            count = int(row[2])
            
            if not names.has_key(name):
                names[name] = [0,0]
            names[name][gender] = names[name][gender] + count
        file.close()
    return names
if __name__ == "__main__":
    getNameList()
